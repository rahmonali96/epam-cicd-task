package uz.jdbc.epamtask.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService {
    private static final Logger logger = LogManager.getLogger(CalculatorService.class);
    public int add(String x, String y) {
        try {
            int xx = Integer.parseInt(x);
            int yy = Integer.parseInt(y);
            return xx + yy;
        } catch (NumberFormatException e) {
            logger.info(e.getMessage());
            throw e;
        }
    }
}
