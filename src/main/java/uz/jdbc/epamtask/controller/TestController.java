package uz.jdbc.epamtask.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.jdbc.epamtask.service.CalculatorService;

@RestController
@RequestMapping("/test")
public class TestController {
    private final CalculatorService calculatorService;

    public TestController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @GetMapping("/calc")
    public int calculate(@RequestParam(name = "x", required = false, defaultValue = "0") String x,
                         @RequestParam(name = "y", required = false, defaultValue = "0") String y) {
        return calculatorService.add(x, y);
    }
}
