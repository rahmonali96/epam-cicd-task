package uz.jdbc.epamtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpamtaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(EpamtaskApplication.class, args);
    }

}
